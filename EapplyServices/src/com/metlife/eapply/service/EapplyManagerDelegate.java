/**
 * 
 */
package com.metlife.eapply.service;

import com.metlife.eapply.dao.EapplyInterfaceManagerDAO;
import com.metlife.eapply.exception.EapplyBaseException;
import com.metlife.eapply.exception.EapplyDAOException;

public class EapplyManagerDelegate implements EapplyManager{
	
	private EapplyInterfaceManagerDAO eapplyInterfaceManagerDAO;
	
	

	public EapplyInterfaceManagerDAO getEapplyInterfaceManagerDAO() {
		return eapplyInterfaceManagerDAO;
	}

	public void setEapplyInterfaceManagerDAO(
			EapplyInterfaceManagerDAO eapplyInterfaceManagerDAO) {
		this.eapplyInterfaceManagerDAO = eapplyInterfaceManagerDAO;
	}	
	@Override
	public boolean validateUser(String permitId) throws EapplyBaseException {
		try{
			return eapplyInterfaceManagerDAO.validateUser(permitId);
		}catch(EapplyDAOException e){
			throw new EapplyBaseException(e);
		}
	}


	/*@Override
	public User retrieveUser(User user) throws IdeaBaseException {
		try{
			return ideaManagerDAO.retrieveUser(user);
		}catch(IdeaDAOException e){
			throw new IdeaBaseException(e);
		}
	}*/

	
	

}