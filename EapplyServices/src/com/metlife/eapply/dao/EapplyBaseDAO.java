/**
 * 
 */
package com.metlife.eapply.dao;

import org.springframework.orm.hibernate3.HibernateTemplate;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;


public class EapplyBaseDAO extends HibernateDaoSupport{
	
	public HibernateTemplate rapHibernateTemplate;

	/**
	 * @return the rmsHibernateTemplate
	 */
	public HibernateTemplate getRapHibernateTemplate() {
		return rapHibernateTemplate;
	}

	/**
	 * @param rmsHibernateTemplate the rmsHibernateTemplate to set
	 */
	public void setRapHibernateTemplate(HibernateTemplate rapHibernateTemplate) {
		this.rapHibernateTemplate = rapHibernateTemplate;
	}

}