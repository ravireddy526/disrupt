package com.metlife.rest.vo;

import java.io.Serializable;

public class ClaimAssessor implements Serializable {


	private String name=null;
	
	private String phone=null;
	private String email=null;
	

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public ClaimAssessor(String name, String phone, String email) {
		super();
		this.name = name;
		this.phone = phone;
		this.email = email;
	}

	
}
