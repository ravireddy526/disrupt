package com.metlife.eapplication.individual.rules;

import org.drools.KnowledgeBase;
import org.drools.KnowledgeBaseFactory;
import org.drools.builder.DecisionTableConfiguration;
import org.drools.builder.DecisionTableInputType;
import org.drools.builder.KnowledgeBuilder;
import org.drools.builder.KnowledgeBuilderError;
import org.drools.builder.KnowledgeBuilderErrors;
import org.drools.builder.KnowledgeBuilderFactory;
import org.drools.builder.ResourceType;
import org.drools.io.ResourceFactory;
import org.drools.logger.KnowledgeRuntimeLogger;
import org.drools.logger.KnowledgeRuntimeLoggerFactory;
import org.drools.runtime.StatelessKnowledgeSession;

import com.metlife.aura.postprocessing.PostProcessRuleBO;
import com.metlife.eapplication.individual.rules.bo.IndvDiscountDataBO;
import com.metlife.eapplication.individual.rules.bo.IndvProductDetailsRuleBO;
import com.metlife.eapplication.individual.rules.bo.IndvReferenceDataBO;
import com.metlife.eapplication.individual.rules.bo.RefProductAuraQuestionFilterBO;
import com.metlife.eapplication.individual.rules.bo.RefPromoCodeRuleBO;

public class EapplicationIndividualRuleHelper{
	
	public static String RULE_FILE_LOC="D:\\newRockingeApp\\_Metlife_Drools\\rules\\eapply\\";
	
	
	public static void main (String args[]){
	
		PostProcessRuleBO ruleBO = new PostProcessRuleBO(); 
		
		ruleBO.setAuraPostProcessID("AURAP0022");
		ruleBO.setOriginalDecision("ACC");
		ruleBO.setBrand("PSUP");
		ruleBO.setTotalExlusion(0);
		ruleBO.setTotalLExlusion(0);
		ruleBO.setTotalMExlusion(0);
		ruleBO.setTotalLoading(50);
		ruleBO.setRuleFileName("PostProcessing2.xls");
		ruleBO = EapplicationIndividualRuleHelper.invokepostProcessRule(ruleBO);
		
		System.out.println("Rule 1 <>>"+ruleBO.getPostAURADecision());
		
	}
	
	
	
	public static void tet_questionFilter(){
		RULE_FILE_LOC="D:\\Metlife_Rules_Eapp\\";
		RefProductAuraQuestionFilterBO questionfilterbo = new RefProductAuraQuestionFilterBO();
		/*questionfilterbo.setBenefitPeriod("2");
		questionfilterbo.setCoverno("IP");*/
		questionfilterbo.setProductCode("MILLI");
		questionfilterbo.setCoverno("192");
		//questionfilterbo.setWaitingPeriod("30");
		questionfilterbo.setRuleFileName("Aura_QuestionFilter.xls");
		questionfilterbo =  invokeRuleProductAuraQuestMapping(questionfilterbo);
		
	}
	public static IndvDiscountDataBO invokeIndvDiscountsRule (IndvDiscountDataBO indvDiscountDataBO) {
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;
		
		try {
			RULE_FILE_LOC = RULE_FILE_LOC + indvDiscountDataBO.getRuleFileName();
			
			if (null != indvDiscountDataBO && null != indvDiscountDataBO.getKbase()) {
				knowledgeBase = indvDiscountDataBO.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			knowledgeSession.execute(indvDiscountDataBO);
		} catch (Throwable throwable) {
			
		}
		return indvDiscountDataBO;
	}
	
	
	public static IndvReferenceDataBO invokeIndvReferenceDataRule (IndvReferenceDataBO indvReferenceDataBO) {
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;
		
		try {
			RULE_FILE_LOC = RULE_FILE_LOC + indvReferenceDataBO.getRuleFileName();
			
			if (null != indvReferenceDataBO && null != indvReferenceDataBO.getKbase()) {
				knowledgeBase = indvReferenceDataBO.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			knowledgeSession.execute(indvReferenceDataBO);
		} catch (Throwable throwable) {
			
		}
		return indvReferenceDataBO;
	}
	
	
	public static PostProcessRuleBO invokepostProcessRule (PostProcessRuleBO postProcessRuleBO) {
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;
		
		
		try {
			RULE_FILE_LOC = RULE_FILE_LOC + postProcessRuleBO.getRuleFileName();
			
			if (null != postProcessRuleBO && null != postProcessRuleBO.getKbase()) {
				knowledgeBase = postProcessRuleBO.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			knowledgeSession.execute(postProcessRuleBO);
		} catch (Throwable throwable) {
			
		}
		return postProcessRuleBO;
	}
	
	
	public static RefPromoCodeRuleBO invokeIndvPromotionCodeRule (RefPromoCodeRuleBO refPromoCodeRuleBO) {
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;

		try {
			RULE_FILE_LOC = RULE_FILE_LOC + refPromoCodeRuleBO.getRuleFileName();
			if (null != refPromoCodeRuleBO && null != refPromoCodeRuleBO.getKbase()) {
				knowledgeBase=refPromoCodeRuleBO.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			
			knowledgeSession.execute(refPromoCodeRuleBO);
		} catch (Throwable throwable) {
			
		}
		return refPromoCodeRuleBO;
	}
	

	public static RefProductAuraQuestionFilterBO invokeRuleProductAuraQuestMapping (RefProductAuraQuestionFilterBO refProdAuraQustFilter) {
		KnowledgeBase knowledgeBase = null;
		StatelessKnowledgeSession knowledgeSession = null;
		KnowledgeRuntimeLogger logger = null;
		
		try {
			RULE_FILE_LOC = RULE_FILE_LOC + refProdAuraQustFilter.getRuleFileName();
			
			if (null != refProdAuraQustFilter && null != refProdAuraQustFilter.getKbase()) {
				knowledgeBase = refProdAuraQustFilter.getKbase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			} else {
				knowledgeBase = readKnowledgeBase();
				knowledgeSession = knowledgeBase.newStatelessKnowledgeSession();
			}
			
			logger = KnowledgeRuntimeLoggerFactory.newFileLogger(knowledgeSession, "UC22BusinessRules");
			
			knowledgeSession.execute(refProdAuraQustFilter);
			logger.close();
		} catch (Throwable throwable) {
			
		}
		return refProdAuraQustFilter;
	}
	
		
		public static IndvProductDetailsRuleBO invokeIndvProductDetailsRules(IndvProductDetailsRuleBO indvProductDetailsRuleBO ){		
			KnowledgeBase kbase=null;
			StatelessKnowledgeSession ksession=null;
			try {
				RULE_FILE_LOC=RULE_FILE_LOC+indvProductDetailsRuleBO.getRuleFileName();
				if(indvProductDetailsRuleBO!=null){
					
					if(indvProductDetailsRuleBO.getKbase()!=null){
						
						kbase=indvProductDetailsRuleBO.getKbase();
						ksession = kbase.newStatelessKnowledgeSession();
					}else{
						
						kbase = readKnowledgeBase();
						ksession = kbase.newStatelessKnowledgeSession();
					}
					
					ksession.execute(indvProductDetailsRuleBO);
				}
			} catch (Throwable t) {
				t.printStackTrace();
			}finally{
				
				if(ksession!=null){
					ksession=null;
				}
			}
			return indvProductDetailsRuleBO;
		}
	
		
		public static KnowledgeBase getRuleSession(String ruleFileLoc,String ruleFileName){
			KnowledgeBase kbase=null;
			try {
				RULE_FILE_LOC = ruleFileLoc+ruleFileName;
				kbase = readKnowledgeBase();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return kbase;
		}
		
	/**
	 * This method will read the rules from the sample.xls file. 
	 * Each row in the excel sheet will represent as a business rule.
	 *  
	 * @return KnowledgeBase object 
	 * @throws Exception
	 */

	private static KnowledgeBase readKnowledgeBase() throws Exception {
		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
		DecisionTableConfiguration config = KnowledgeBuilderFactory.newDecisionTableConfiguration();
		config.setInputType(DecisionTableInputType.XLS);
	
		kbuilder.add(ResourceFactory.newFileResource(RULE_FILE_LOC), ResourceType.DTABLE, config);
		KnowledgeBuilderErrors errors = kbuilder.getErrors();
		
		if (errors.size() > 0) {
			for (KnowledgeBuilderError error: errors) {
				System.err.println(error);
			}
			throw new IllegalArgumentException("Could not parse knowledge.");
		}
		
		//KnowledgeBaseConfiguration kConfig = KnowledgeBaseFactory.newKnowledgeBaseConfiguration();
		//kConfig.setOption(MBeansOption.ENABLED);
		KnowledgeBase kbase = KnowledgeBaseFactory.newKnowledgeBase();
		kbase.addKnowledgePackages(kbuilder.getKnowledgePackages());
		
		
		kbuilder = null;
		config = null;
		errors= null;
		
		return kbase;
	}

}