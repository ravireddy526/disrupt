package com.test.VO;

import java.io.Serializable;

public class User implements Serializable {

	private String fname=null;
	
	private String lname=null;

	public String getFname() {
		return fname;
	}

	public void setFname(String fname) {
		this.fname = fname;
	}

	public String getLname() {
		return lname;
	}

	public User(String fname, String lname) {
		super();
		this.fname = fname;
		this.lname = lname;
	}

	public void setLname(String lname) {
		this.lname = lname;
	}
}
