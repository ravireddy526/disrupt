/**
 *
 */
package com.metlife.eapplication.individual.rules.bo;

import org.drools.KnowledgeBase;

/**
 * @author 140663
 * 
 */
public class IndvDiscountDataBO {

	public String productCode = null;

	public String applicationType = null;

	public String staffDiscount = null;
	
	public String familyDiscount = null;
	
	public String jointDiscount = null;
	
	public String discountCode = null;
	
	public String flybuysDiscount = null;

	public String ruleFileName=null;
	
	public KnowledgeBase kbase = null;

	public KnowledgeBase getKbase() {
		return kbase;
	}

	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}
	public String getRuleFileName() {
		return ruleFileName;
	}

	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getFamilyDiscount() {
		return familyDiscount;
	}

	public void setFamilyDiscount(String familyDiscount) {
		this.familyDiscount = familyDiscount;
	}

	public String getJointDiscount() {
		return jointDiscount;
	}

	public void setJointDiscount(String jointDiscount) {
		this.jointDiscount = jointDiscount;
	}

	public String getStaffDiscount() {
		return staffDiscount;
	}

	public void setStaffDiscount(String staffDiscount) {
		this.staffDiscount = staffDiscount;
	}

	public String getDiscountCode() {
		return discountCode;
	}

	public void setDiscountCode(String discountCode) {
		this.discountCode = discountCode;
	}

	public String getFlybuysDiscount() {
		return flybuysDiscount;
	}

	public void setFlybuysDiscount(String flybuysDiscount) {
		this.flybuysDiscount = flybuysDiscount;
	}
	
}
