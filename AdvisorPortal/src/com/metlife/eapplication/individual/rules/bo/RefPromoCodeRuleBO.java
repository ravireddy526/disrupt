package com.metlife.eapplication.individual.rules.bo;

import org.drools.KnowledgeBase;

public class RefPromoCodeRuleBO {
	
	private String productCode;
	private String promotionCode;
	private String startDate;
	private String endDate;	
	private String magazines;
	private String coverNumber;
	private String coverAmount;
	private String coverDescription;
	private boolean melanomaApplied = false;
	private boolean magazineApplied = false;
	private boolean cashbackApplied = false;
	private boolean crosssellApplied = false;
	
	public String ruleFileName=null;
	
	public String successMsg = null;
	
	public String imgPath = null;
	
	public boolean flybuysDiscount = false;
	
//	public StatelessKnowledgeSession kSession=null;
	
	public KnowledgeBase kbase = null;
	public KnowledgeBase getKbase() {
		return kbase;
	}
	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}
	public String getRuleFileName() {
		return ruleFileName;
	}
	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}
	public String getCoverAmount() {
		return coverAmount;
	}
	public void setCoverAmount(String coverAmount) {
		this.coverAmount = coverAmount;
	}
	public String getCoverDescription() {
		return coverDescription;
	}
	public void setCoverDescription(String coverDescription) {
		this.coverDescription = coverDescription;
	}
	public String getCoverNumber() {
		return coverNumber;
	}
	public void setCoverNumber(String coverNumber) {
		this.coverNumber = coverNumber;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public boolean isMagazineApplied() {
		return magazineApplied;
	}
	public void setMagazineApplied(boolean magazineApplied) {
		this.magazineApplied = magazineApplied;
	}
	public String getMagazines() {
		return magazines;
	}
	public void setMagazines(String magazines) {
		this.magazines = magazines;
	}
	public boolean isMelanomaApplied() {
		return melanomaApplied;
	}
	public void setMelanomaApplied(boolean melanomaApplied) {
		this.melanomaApplied = melanomaApplied;
	}
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public String getPromotionCode() {
		return promotionCode;
	}
	public void setPromotionCode(String promotionCode) {
		this.promotionCode = promotionCode;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public boolean isCashbackApplied() {
		return cashbackApplied;
	}
	public void setCashbackApplied(boolean cashbackApplied) {
		this.cashbackApplied = cashbackApplied;
	}
	public boolean isCrosssellApplied() {
		return crosssellApplied;
	}
	public void setCrosssellApplied(boolean crosssellApplied) {
		this.crosssellApplied = crosssellApplied;
	}
	public String getSuccessMsg() {
		return successMsg;
	}
	public void setSuccessMsg(String successMsg) {
		this.successMsg = successMsg;
	}
	public String getImgPath() {
		return imgPath;
	}
	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	public boolean isFlybuysDiscount() {
		return flybuysDiscount;
	}
	public void setFlybuysDiscount(boolean flybuysDiscount) {
		this.flybuysDiscount = flybuysDiscount;
	}

	
}