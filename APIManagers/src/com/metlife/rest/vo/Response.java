package com.metlife.rest.vo;

import java.io.Serializable;
import java.util.List;

public class Response implements Serializable {


	private String code=null;	
	private String errorMsg = null;
	private List<ClaimStatus> claimStatusList = null;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public List<ClaimStatus> getClaimStatusList() {
		return claimStatusList;
	}
	public void setClaimStatusList(List<ClaimStatus> claimStatusList) {
		this.claimStatusList = claimStatusList;
	}


	public Response(String code, String errorMsg, List<ClaimStatus> claimStatusList ) {
		super();
		this.code = code;
		this.errorMsg = errorMsg;
		this.claimStatusList = claimStatusList;		
	}

	
}
