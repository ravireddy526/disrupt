/**
 *
 */
package com.metlife.eapplication.individual.rules.bo;

import org.drools.KnowledgeBase;

/**
 * @author 140663
 * 
 */
public class IndvReferenceDataBO {

	public String referenceTitle = null;

	public String referenceState = null;

	public String referenceContact = null;

	public String referenceFrequency = null;
	
	public String referenceData = null;
	
	public String referencePreferedTime = null;
	
	public String referenceMonthSeries = null;
	
	public String ruleFileName=null;
	
	public String referencePolicyDuration=null;
	
	public KnowledgeBase kbase = null;

	public KnowledgeBase getKbase() {
		return kbase;
	}

	public void setKbase(KnowledgeBase kbase) {
		this.kbase = kbase;
	}

	public String getReferenceContact() {
		return referenceContact;
	}

	public void setReferenceContact(String referenceContact) {
		this.referenceContact = referenceContact;
	}

	public String getReferenceData() {
		return referenceData;
	}

	public void setReferenceData(String referenceData) {
		this.referenceData = referenceData;
	}

	public String getReferenceFrequency() {
		return referenceFrequency;
	}

	public void setReferenceFrequency(String referenceFrequency) {
		this.referenceFrequency = referenceFrequency;
	}

	public String getReferenceState() {
		return referenceState;
	}

	public void setReferenceState(String referenceState) {
		this.referenceState = referenceState;
	}

	public String getReferenceTitle() {
		return referenceTitle;
	}

	public void setReferenceTitle(String referenceTitle) {
		this.referenceTitle = referenceTitle;
	}

	public String getRuleFileName() {
		return ruleFileName;
	}

	public void setRuleFileName(String ruleFileName) {
		this.ruleFileName = ruleFileName;
	}

	public String getReferencePreferedTime() {
		return referencePreferedTime;
	}

	public void setReferencePreferedTime(String referencePreferedTime) {
		this.referencePreferedTime = referencePreferedTime;
	}

	public String getReferenceMonthSeries() {
		return referenceMonthSeries;
	}

	public void setReferenceMonthSeries(String referenceMonthSeries) {
		this.referenceMonthSeries = referenceMonthSeries;
	}

	public String getReferencePolicyDuration() {
		return referencePolicyDuration;
	}

	public void setReferencePolicyDuration(String referencePolicyDuration) {
		this.referencePolicyDuration = referencePolicyDuration;
	}

	
}
